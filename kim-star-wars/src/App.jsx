import styles from './App.module.css';

import {Route, Routes} from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import PeoplePage from "./pages/PeoplePage/PeoplePage";
import Header from "./components/Header/Header";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";
import PersonPage from "./pages/PersonPage/PersonPage";
import FavoritePage from "./pages/favoritePage/FavoritePage";
import SearchPage from "./pages/SearchPage/SearchPage";


const App = () => {
    return (
        <div className={styles.wrapper}>
            <Header/>
            <Routes>
                <Route path={'/'} element = {<HomePage/>}/>
                <Route path={'/people'} element = {<PeoplePage/>}/>
                <Route path={'/people/:id'} element = {<PersonPage/>}/>
                <Route path={'/not-found'} element = {<NotFoundPage/>}/>
                <Route path={'*'} element = {<NotFoundPage/>}/>
                <Route path={'/favorites'} element = {<FavoritePage/>}/>
                <Route path={'/search'} element = {<SearchPage/>}/>
            </Routes>

        </div>

    );
};


export default App;
