import styles from './NotFoundPage.module.css';
import {useLocation} from "react-router-dom";

const NotFoundPage = () => {
    let location = useLocation();


    return(
        <div>
            <img className={styles.img} src="https://img.freepik.com/free-vector/glitch-error-404-page_23-2148105404.jpg" alt="Not found"/>
            <p className={styles.text}> No match for {location.pathname}</p>
        </div>
    )
};
export default NotFoundPage

