import {useState, useEffect} from "react";
import styles from './PeoplePage.module.css';
import {API_PEOPLE} from "../../constants/api";
import {getApiResource, changeHTTP} from "../../utils/network";
import {getPeopleId, getPeopleImage, getPeoplePageId} from "../../services/getPeopleData";
import PeopleList from "../../components/PeoplePage/PeopleList/PeopleList";
import {WithErrorApi} from "../../hoc-helpers/WithErrorApi";
import {UseQueryParams} from "../../hooks/useQueryParams";
import PeopleNavigation from "../../components/PeoplePage/PeopleNavigation/PeopleNavigation";

const PeoplePage = ({setErrorApi}) => {

    const [people, setPeople] = useState(null);
    const [prevPage, setPrevPage] = useState(null);
    const [nextPage, setNextPage] = useState(null);
    const [counterPage, setCounterPage] = useState(1);

    const query = UseQueryParams();
    const queryPage = query.get('page');

    const getResource = async (url) => {
        const res = await getApiResource(url);

        if (res) {
            const peopleList = res.results.map(({name, url}) => {
                const id = getPeopleId(url);
                const img = getPeopleImage(id);
                return {
                    name,
                    id,
                    img
                };
            });
            setPeople(peopleList);
            setPrevPage(changeHTTP(res.previous));
            setNextPage(changeHTTP(res.next));
            setCounterPage(getPeoplePageId(url));
            setErrorApi(false);
        } else {
            setErrorApi(true);
        }

    };

    useEffect(() => {
        getResource(API_PEOPLE + queryPage);
    }, [queryPage]);

    return (
        <>
           <PeopleNavigation
               getResource = {getResource}
               prevPage = {prevPage}
               nextPage = {nextPage}
               counterPage = {counterPage}
           />
            {people && <PeopleList people={people}/>}
        </>
    );
};


export default WithErrorApi(PeoplePage);

//
// return (
//     <div className={styles.people}>
//         {errorApi
//             ?< h2 > Error < /h2>
//             : (<>
//                 <h1>Navigation</h1>
//                 {people && <PeopleList people={people}/>}
//             </>)
//         }
//
//     </div>
// );