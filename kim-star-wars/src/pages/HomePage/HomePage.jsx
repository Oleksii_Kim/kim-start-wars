import styles from './HomePage.module.css'
import ChooseSide from "../../components/HomePage/ChooseSide/ChooseSide";


const HomePage = ()=>{

    return (
        <div className='header__text'>
            <ChooseSide/>
        </div>
    )
}

export default HomePage