import styles from './FavoritePage.module.css'
import {useSelector} from "react-redux";
import {useEffect, useState} from "react";
import PeopleList from "../../components/PeoplePage/PeopleList/PeopleList";

const FavoritePage = ()=>{
    const storeData = useSelector(state=>state.favorites.inFavorite)

    const [people,setPeople] = useState([])

    useEffect(()=>{
        if (storeData.length){
            setPeople(storeData)
        }
    },[storeData])

    return (
        <div>
            {people.length
                ? <PeopleList people={people}/>
                : <h2 className={styles.comment}> No Data</h2>}

        </div>
    )
}
export default FavoritePage