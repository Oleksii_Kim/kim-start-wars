import {createSlice} from "@reduxjs/toolkit";
import {getLocalStorage} from "../../utils/localStorage";


const favoriteSlice = createSlice({
    name:'favorite',
    initialState:{
        inFavorite:getLocalStorage('store')
    },

    reducers:{
        addToFavorite:(state, action) => {
            state.inFavorite.push(action.payload);
        },
        deleteFromFavorite: (state, action) => {
            state.inFavorite = state.inFavorite.filter(person => person.id !== action.payload)
},

    }


})
export const {addToFavorite,deleteFromFavorite} = favoriteSlice.actions

export default favoriteSlice.reducer