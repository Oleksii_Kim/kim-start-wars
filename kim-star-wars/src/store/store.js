import {configureStore} from "@reduxjs/toolkit";
import favoriteReducer from "./favorite/favoriteSlice";
import {setLocalStorage} from "../utils/localStorage";

export const store = configureStore({
    reducer:{
        favorites: favoriteReducer,
    }
})

store.subscribe(()=>{
    setLocalStorage('store', store.getState().favorites.inFavorite)
})