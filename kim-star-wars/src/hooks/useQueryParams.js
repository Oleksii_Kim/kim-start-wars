import {useLocation} from "react-router-dom";



export const UseQueryParams = () => {
    return new URLSearchParams(useLocation().search)
};