export const HTTPS = 'https://';
export const HTTP = 'http://';


export const SWAPI_ROOT = 'swapi.dev/api/';
export const SWAPI_PEOPLE = 'people';
export const SWAPI_PARAM_PAGE = '/?page=';
export const SWAPI_PARAM_SEARCH = '/?search=';


export const API_PEOPLE = HTTPS + SWAPI_ROOT + SWAPI_PEOPLE+SWAPI_PARAM_PAGE;
export const API_PERSON = HTTPS + SWAPI_ROOT + SWAPI_PEOPLE;
export const API_SEARCH = HTTPS + SWAPI_ROOT + SWAPI_PEOPLE + SWAPI_PARAM_SEARCH;


const GUITE_ROOT_IMG = `https://starwars-visualguide.com/assets/img/`;
const GUITE_PEOPLE = 'characters';
export const GUITE_IMG_EXTENSIONS = '.jpg';

export const URL_IMG_PERSON = GUITE_ROOT_IMG + GUITE_PEOPLE;
