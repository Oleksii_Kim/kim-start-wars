import {useState} from "react";
import ErrorMessage from '../components/ErrorMessage/ErrorMessage'

export const WithErrorApi = (View) => {
    // const [errorApi, setErrorApi] = useState(false);
    let errorApi = false
    const setErrorApi = (value)=>{
        errorApi = value
    }
    return (props => {
        return (
            <>
                {errorApi
                    ? <ErrorMessage/>
                    :
                        <View
                            setErrorApi = {setErrorApi}
                            {...props}
                        />

                }

            </>
        );
    });
};