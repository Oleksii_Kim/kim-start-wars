import styles from './ChooseSide.module.css'
import {useTheme,THEME_LIGHT,THEME_DARK,THEME_NEUTRAL} from "../../../context/ThemeProvider";
import cn from 'classnames'

import imgLightSide from './images/light-side.jpeg'
import imgDarkSide from './images/dark-side.jpeg'
import imgFalcon from './images/falcon.jpeg'


const ChooseSideItem  =({theme,text,img,classes})=>{
    const isTheme = useTheme()
    return (
        <div className={cn(styles.item,classes)} onClick={()=>isTheme.change(theme)}>
            <div className={styles.item__header}>{text}</div>
            <img className={styles.item__img} src={img} alt={text}/>

        </div>
        )
}



const ChooseSide = ()=>{
    const elements = [
        {
            classes:styles.item__light,
            theme:THEME_LIGHT,
            text:"Light Side",
            img:imgLightSide
        },
        {
            classes:styles.item__dark,
            theme:THEME_DARK,
            text:"Dark Side",
            img:imgDarkSide
        },
        {
            classes:styles.item__neutral,
            theme:THEME_NEUTRAL,
            text:"I am Solo",
            img:imgFalcon
        }
    ]

    return(
        <div className={styles.container}>
            {elements.map( ({theme,text,img,classes},index) => (
                <ChooseSideItem
                    key={index}
                    theme={theme}
                    text={text}
                    img={img}
                    classes={classes}
                />
                ))
            }

            {/*<ChooseSideItem theme={THEME_LIGHT} text='Light Side' img={imgLightSide}/>*/}
            {/*<ChooseSideItem theme={THEME_DARK} text='Dark Side' img={imgDarkSide}/>*/}
            {/*<ChooseSideItem theme={THEME_NEUTRAL} text='I am Solo' img={imgFalcon}/>*/}
        </div>
    )
}
export default ChooseSide