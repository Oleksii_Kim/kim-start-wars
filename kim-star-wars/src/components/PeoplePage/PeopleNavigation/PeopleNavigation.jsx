import styles from './PeopleNavigation.module.css';
import {Link} from "react-router-dom";
import UiButton from '../../UI/UiButton/UiButton'

const PeopleNavigation = ({
                              getResource,
                              prevPage,
                              nextPage,
                              counterPage
                          }) => {

    const handleChangeNext = ()=> getResource(nextPage)
    const handleChangePrev = ()=> getResource(prevPage)

    return (
        <div className={styles.container}>
            <Link className={styles.buttons} to={`/people/?page=${counterPage - 1}`}>
                <UiButton
                    text = 'Previous'
                    disabled={!prevPage}
                    onClick = {handleChangePrev}
                />
            </Link>
            <Link className={styles.buttons} to={`/people/?page=${counterPage + 1}`}>
                <UiButton
                    text = 'Next'
                    disabled={!nextPage}
                    onClick = {handleChangeNext}
                />
            </Link>
        </div>
    );
};
export default PeopleNavigation;