import styles from './Favorite.module.css';
import icon from './img/bookmark.svg';
import {Link} from "react-router-dom";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";

const Favorite = () => {
    const[count,setCount]= useState(null)
    const storeData = useSelector(state=> state.favorites.inFavorite)

    useEffect(()=>{
        const length = storeData.length
        setCount(length)
    },[setCount,storeData.length])

    return (
        <div className={styles.container}>
            <Link to="/favorites" >
                <span className={styles.counter}>{count}</span>
                <img className={styles.icon} src={icon} alt='favorites'/>
            </Link>

        </div>
    );
};

export default Favorite