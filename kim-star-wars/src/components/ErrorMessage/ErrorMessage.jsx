import styles from './ErrorMessage.module.css'

const ErrorMessage= ()=>{
    return(
        <>
            <p className={styles.text1}>
                We cannot display data.
                Come back when we fix everything
            </p>
            <p className={styles.text2}>
                Come back when we fix everything
            </p>
        </>
    )
}
export default ErrorMessage