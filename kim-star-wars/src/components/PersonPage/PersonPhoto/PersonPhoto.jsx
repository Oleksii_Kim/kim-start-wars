import styles from './PersonPhoto.module.css';
import {useDispatch} from "react-redux";
import {addToFavorite, deleteFromFavorite} from "../../../store/favorite/favoriteSlice";
import iconFavorite from './img/favorite.svg';
import iconFavoriteFill from './img/favorite-fill.svg';

const PersonPhoto = ({
                         personId,
                         personPhoto,
                         personName,
                         personFavorite,
                         setPersonFavorite
                     }) => {
    const dispatch = useDispatch();

    const dispatchFavoritePeople = () => {
        if (personFavorite) {
            dispatch(deleteFromFavorite(personId));
            setPersonFavorite(false);
        } else {
            dispatch(addToFavorite({
                id: personId,
                name: personName,
                img: personPhoto
            }));
            setPersonFavorite(true);
        }
    };

    return (
        <>
            <div className={styles.container}>
                <img className={styles.photo} src={personPhoto} alt={personName}/>
                <img src={personFavorite ? iconFavoriteFill : iconFavorite}
                     alt=""
                     onClick={dispatchFavoritePeople}
                     className={styles.favorite}
                />
            </div>
        </>
    );
};

export default PersonPhoto;