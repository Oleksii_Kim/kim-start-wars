import styles from './PersonLinkBack.module.css'
import iconBack from './img/arrow-left.png'
import { useNavigate } from 'react-router-dom';

const PersonLinkBack =()=>{

    const navigate = useNavigate()

    const handleGoBack = e =>{
        e.preventDefault()
        navigate(-1)

    }

    return(
        <a href="a"
        onClick={handleGoBack}
        className={styles.link}>
            <img className={styles.link__img} src={iconBack} alt="Go back"/>
            <span>Go back</span>
        </a>
    )
}

export default PersonLinkBack