import styles from './UiButton.module.css';
import '../index.css';
import classNames from "classnames";

const UiButton = ({
                      text,
                      onClick,
                      disabled,
                      theme = 'dark',
                      classes
                  }) => {
    return (
        <button
            disabled={disabled}
            onClick={onClick}
            className={classNames(styles.button, styles[theme], classes)}
        >
            {text}
        </button>
    );
};

export default UiButton;